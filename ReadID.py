#!/usr/bin/env python3

import RPi.GPIO as GPIO
import signal,LED,MFRC522,requests,os
from time import sleep
	
def uidToString(uid):
    mystring = ""
    for i in uid:
        mystring = format(i, '02X') + mystring
    return mystring


def end_read(signal, frame):
    global continue_reading
    print("Ctrl+C captured, ending read.")
    continue_reading = False
    GPIO.cleanup()

continue_reading = True

signal.signal(signal.SIGINT, end_read)
MIFAREReader = MFRC522.MFRC522()

# Welcome message
print("Welcome to the VidaBox ReadID")
print("Press Ctrl-C to stop.")

# This loop keeps checking for chips.
# If one is near it will get the UID and authenticate
while continue_reading:

    (status, TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)	
	
    if status == MIFAREReader.MI_OK:
		(status, uid) = MIFAREReader.MFRC522_SelectTagSN()
		if status == MIFAREReader.MI_OK:
			print("Chip ID: %s" % uidToString(uid))
		else:
			print("Authentication error")
