#!/usr/bin/env python3

import signal,LED,MFRC522,requests,os,subprocess,ast,serial
from time import sleep
from time import gmtime, strftime
import pygame

mediaLink = ''
startSound = ''
shutdownTime = ''
playOnFigur = ''
	
def getBoxConfigFromDb():
	dbConfigData = getChipsFromServer().split("*")[1]
	count = 0
	dbConfigDataArray = dbConfigData.split("&")
		
	return dbConfigDataArray
	
def getChipconfigFromDb():
	file = open("/home/pi/vidabox/conf.TXT", "r")
	if file.mode == 'r':
		configFile = file.read()
		chipConfig = ast.literal_eval(configFile)
	else:
		chipConfig = {'name': 'demo', 'password': 'demo', 'version': '2'}
	return chipConfig
	
def getChipsFromServer():
	chipConfig = getChipconfigFromDb()
	r = requests.get('http://vida-tec.de/get/userData.php', headers={'X-Platform': 'VidaBox'}, params={'version': chipConfig['version'],'name': chipConfig['name'],'password': chipConfig['password']})
	r.headers
	return (r.text)

def getChipsArray(string):
	dbChipData = getChipsFromServer().split("*")[0]
	chips = dbChipData.split("|")
	chip = chips[0].split("&")
	count = 0
	chipArray = []
	
	for value in chips:
		chipArray.append(value.split("&"))
		count = count+1
		
	return chipArray
	
def getValueByName(name,chipsArray):
	for value in chipsArray:
		if str(value[0]) == str(name):
			return(value)
	return 'null'
	
def uidToString(uid):
    uidString = ""
    for i in uid:
        uidString = format(i, '02X') + uidString
    return uidString

def playAudio(url):
	stream_url = mediaLink+str(url)
	pygame.mixer.init()
	r = requests.get(stream_url,stream=True)
	pygame.mixer.music.load(r.raw)
	pygame.mixer.music.play()

def startUpSound():
	if startSound.count('.mp3'):
		playAudio(startSound)

def checkShutdownTime():
	if shutdownTime != '0':	
		timeNow = str(subprocess.check_output(["sudo","date","+%H:%M"]).split('\n')[0])
		if str(shutdownTime) == timeNow:
			print('DOWN')
			down = subprocess.check_output(["sudo","shutdown","-h","now"])
def pauseMedia(playerState):
	if playOnFigur == '1':
		LED.greenON()
		pygame.mixer.music.pause()
		return 'pause'
	else:
		return playerState
		
def LoadVidaBoxConfig():
	global mediaLink
	global startSound
	global shutdownTime
	global playOnFigur
	BoxConfig = getBoxConfigFromDb()
	mediaLink = BoxConfig[0]
	startSound = BoxConfig[1]
	shutdownTime = BoxConfig[2]
	playOnFigur = BoxConfig[3]
	print(BoxConfig)
	

