from gpiozero import LED
from time import sleep

ledColor = LED(16)
ledGreen = LED(20)

def greenON():
	ledColor.off()
	ledGreen.on()
	
def greenOFF():
	ledGreen.off()
	
def colorON():
	ledGreen.off()
	ledColor.on()
	
def colorOFF():
	ledColor.off()
	
def startMedia():
	ledColor.off()
#ERROR 2 - RFID fail
#ERROR 3 - login fail
#ERROR 5 - non chipsArray

def error(times):
	i = 0
	ledColor.off()
	ledGreen.off()
	while i < int(times):
		sleep(0.5)
		ledGreen.on()
		sleep(0.5)
		ledGreen.off()
		i += 1
	print('Error: '+str(times))
	sleep(2)

	
def startNewFigur():
	greenON()
	sleep(0.1)
	greenOFF()
	sleep(0.1)
	greenON()
	sleep(0.1)
	greenOFF()
